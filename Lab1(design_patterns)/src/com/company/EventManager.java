package com.company;

import java.util.ArrayList;
import java.util.HashMap;

public class EventManager {
    private static HashMap<String, ArrayList<Listener>> listeners = new HashMap<>();

    static void subscribe(String eventType, Listener listener) {
        ArrayList<Listener> a = listeners.get(eventType);
        if (a != null) {
            a.add(listener);
        } else {
            a = new ArrayList<>();
            a.add(listener);
            listeners.put(eventType, a);
        }
    }

    public static void unsubscribe(String eventType, Listener listener) {
        ArrayList<Listener> a = listeners.get(eventType);
        if (a != null) {
            a.remove(listener);
        }
    }

    public static void notify(String eventType, Object ...data) {
        ArrayList<Listener> a = listeners.get(eventType);
        if (a != null) {
            a.forEach((listener -> listener.update(data)));
        }
    }
}
