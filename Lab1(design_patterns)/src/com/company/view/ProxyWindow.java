package com.company.view;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class ProxyWindow implements UrlLoader {
    private Window window;
    private ArrayList<URL> blockedHosts = new ArrayList<>();

    public ProxyWindow(Window window) {
        this.window = window;
        fillBlockedURL();
    }

    private void fillBlockedURL() {
        try {
            blockedHosts.add(new URL("http://gmail.com"));
            blockedHosts.add(new URL("http://facebook.com"));
        } catch (MalformedURLException ignored) {
        }
    }

    @Override
    public void loadUrl(String url) {
        if (canAccess(url)) {
            window.loadUrl(url);
        } else {
            System.out.println("Restricted " + url);
            window.loadUrl(null);
        }
    }

    private boolean canAccess(String url) {
        try {
            String host = new URL(url).getHost();
            if (host.startsWith("www.")) {
                host = host.substring(4);
            }
            String finalHost = host;
            return blockedHosts.stream().noneMatch((el) -> finalHost.equals(el.getHost()));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void updateState(String url) {
        window.updateState(url);
    }
}
