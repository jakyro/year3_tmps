package com.company.view;

public interface UrlLoader {

    void loadUrl(String url);

    void updateState(String url);
}