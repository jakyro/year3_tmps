package com.company.view;

import com.company.EventManager;
import com.company.view.bridge.*;
import com.company.view.header.HeaderButtonFactory;
import com.company.view.state.HeaderContext;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.HashMap;

public class Window implements UrlLoader{
    public static final String FORWARD_BUTTON = "FORWARD";
    public static final String BACK_BUTTON = "BACK";
    public static final String GO_BUTTON = "GO";
    public static final String OK_BUTTON = "OK";

    private JEditorPane webView;
    private HashMap<String, JButton> buttons = new HashMap<>();
    private JTextField inputField;
    private Thread thread;
    private HeaderContext state = new HeaderContext(this);
    public Window() {
        JFrame frame = new JFrame("Frame");
        webView = new JEditorPane();
        webView.setEditable(false);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setLayout(new BorderLayout());
        frame.add(createHeader(), BorderLayout.NORTH);
        frame.add(webView, BorderLayout.CENTER);

        frame.setMinimumSize(new Dimension(300, 400));
        frame.setSize(600, 600);
        frame.setVisible(true);
    }

    private Panel createHeader() {
        Panel panel = new Panel();

        inputField = createInputField();
        UrlFieldDecorator urlFieldDecorator = new UrlFieldDecorator(inputField);

        JButton forwardButton = HeaderButtonFactory.createButton(FORWARD_BUTTON);
        forwardButton.addActionListener(actionListener -> EventManager.notify(FORWARD_BUTTON));

        JButton goButton = HeaderButtonFactory.createButton(GO_BUTTON);
        goButton.addActionListener(actionListener -> EventManager.notify(GO_BUTTON, urlFieldDecorator.getText()));

        JButton backButton = HeaderButtonFactory.createButton(BACK_BUTTON);
        backButton.addActionListener(actionListener -> EventManager.notify(BACK_BUTTON));

        final JComboBox<Bookmark> bk = new JComboBox<>();
        bk.setRenderer(new BookmarkItem());
        GoogleBookmark googleBookmark = new GoogleBookmark();
        YoutubeBookmark youtubeBookmark = new YoutubeBookmark();
        bk.addItem(googleBookmark);
        bk.addItem(youtubeBookmark);
        bk.setMaximumSize(bk.getPreferredSize());
        bk.setAlignmentY(Component.BOTTOM_ALIGNMENT);

        JButton okButton = HeaderButtonFactory.createButton(OK_BUTTON);
        okButton.addActionListener(e -> {
            Bookmark bookmark = bk.getItemAt(bk.getSelectedIndex());
            updateState(bookmark.getBookmarkUrl());
            EventManager.notify(GO_BUTTON, urlFieldDecorator.getText());
        });

        buttons.put(BACK_BUTTON, backButton);
        buttons.put(GO_BUTTON, goButton);
        buttons.put(FORWARD_BUTTON, forwardButton);
        buttons.put(OK_BUTTON, okButton);

        JLabel bkLabel=new JLabel("Bookmarks    ", JLabel.CENTER);


        Panel eastPanel = createPanelWithChildren(new FlowLayout(), forwardButton, goButton);
        Panel westPanel = createPanelWithChildren(new FlowLayout(), backButton);
        Panel bookmarks = createPanelWithChildren(new FlowLayout(), bkLabel, bk,okButton);

        panel.setLayout(new BorderLayout());
        panel.add(inputField, BorderLayout.CENTER);
        panel.add(eastPanel, BorderLayout.EAST);
        panel.add(westPanel, BorderLayout.WEST);
        panel.add(bookmarks, BorderLayout.SOUTH);

        return panel;
    }

    private JTextField createInputField() {
        return new JTextField();
    }

    private Panel createPanelWithChildren(LayoutManager layout, Component... components) {
        Panel panel = new Panel();
        panel.setLayout(layout);
        for (Component object : components) {
            panel.add(object);
        }
        return panel;
    }

    public void loadUrl(String url) {
        if (thread != null && thread.isAlive()) {
            thread.interrupt();
        }
        if (url == null) {
            loadAccessDeniedPage();
            return;
        }
        thread = new Thread(() -> {
            try {
                webView.setPage(url);
            } catch (IOException e) {
                webView.setContentType("text/html");
                webView.setText("<html>Could not load</html>");
            }
        });
        thread.start();
    }

    private void loadAccessDeniedPage() {
        webView.setContentType("text/html");
        webView.setText("<html>Access restricted!</html>");
    }

    public void updateState(String url) {
        this.setUrl(url);
        state.setState();
    }

    public void disableButton(String name) {
        buttons.get(name).setEnabled(false);
    }

    public void enableButton(String name) {
        buttons.get(name).setEnabled(true);
    }

    public void setUrl(String url) {
        inputField.setText(url);
    }
}