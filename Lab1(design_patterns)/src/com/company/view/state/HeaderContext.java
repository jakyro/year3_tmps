package com.company.view.state;

import com.company.history.History;
import com.company.view.Window;

public class HeaderContext {
    private CacheState state;
    private Window window;

    public HeaderContext(Window window) {
        this.window = window;
        state = new CacheState(new NoPageCachedState(),
                               new OnePageCashedState(),
                               new FirstPageOpenedState(),
                               new LastPageCachedOpenedState(),
                               new TwoOrMorePagesCachedState());
    }

    public void setState() {
        if (History.getInstance().isNoPagesCached()) {
            state.noPageSetNavigationButtonVisibility(window);
        } else if (History.getInstance().isOnePageCached()) {
            state.onePageSetNavigationButtonVisibility(window);
        } else if (History.getInstance().isFirst()) {
            state.firstPageSetNavigationButtonVisibility(window);
        } else if (History.getInstance().isLast()) {
            state.lastPageSetNavigationButtonVisibility(window);
        } else {
            state.twoOrMorePagesSetNavigationButtonVisibility(window);
        }
    }
}
