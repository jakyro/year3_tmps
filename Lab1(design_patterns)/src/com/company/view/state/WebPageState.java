package com.company.view.state;

import com.company.view.Window;

interface WebPageState {
    void setNavigationButtonVisibility(Window window);
}
