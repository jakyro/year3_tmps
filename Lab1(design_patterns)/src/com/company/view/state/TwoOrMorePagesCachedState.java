package com.company.view.state;

import com.company.view.Window;

public class TwoOrMorePagesCachedState implements WebPageState {

    @Override
    public void setNavigationButtonVisibility(Window window) {
        window.enableButton(Window.BACK_BUTTON);
        window.enableButton(Window.FORWARD_BUTTON);
    }
}
