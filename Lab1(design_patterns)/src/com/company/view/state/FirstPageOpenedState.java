package com.company.view.state;

import com.company.view.Window;

public class FirstPageOpenedState implements WebPageState {

    @Override
    public void setNavigationButtonVisibility(Window window) {
        window.disableButton(Window.BACK_BUTTON);
        window.enableButton(Window.FORWARD_BUTTON);
    }
}
