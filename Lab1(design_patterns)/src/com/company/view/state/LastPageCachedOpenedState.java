package com.company.view.state;

import com.company.view.Window;

public class LastPageCachedOpenedState implements WebPageState {

    @Override
    public void setNavigationButtonVisibility(Window window) {
        window.enableButton(Window.BACK_BUTTON);
        window.disableButton(Window.FORWARD_BUTTON);
    }
}
