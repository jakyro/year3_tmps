package com.company.view.state;

import com.company.view.Window;

public class CacheState {
    private NoPageCachedState noPageCachedState;
    private OnePageCashedState onePageCashedState;
    private FirstPageOpenedState firstPageOpenedState;
    private LastPageCachedOpenedState lastPageCachedOpenedState;
    private TwoOrMorePagesCachedState twoOrMorePagesCachedState;

    public CacheState(NoPageCachedState noPageCachedState,
                           OnePageCashedState onePageCashedState,
                           FirstPageOpenedState firstPageOpenedState,
                           LastPageCachedOpenedState lastPageCachedOpenedState,
                           TwoOrMorePagesCachedState twoOrMorePagesCachedState)
    {
        this.noPageCachedState = noPageCachedState;
        this.onePageCashedState = onePageCashedState;
        this.firstPageOpenedState = firstPageOpenedState;
        this.lastPageCachedOpenedState = lastPageCachedOpenedState;
        this.twoOrMorePagesCachedState = twoOrMorePagesCachedState;
    }

    public void noPageSetNavigationButtonVisibility(Window window) {
        noPageCachedState.setNavigationButtonVisibility(window);
    }

    public void onePageSetNavigationButtonVisibility(Window window) {
        onePageCashedState.setNavigationButtonVisibility(window);
    }

    public void firstPageSetNavigationButtonVisibility(Window window) {
        firstPageOpenedState.setNavigationButtonVisibility(window);
    }

    public void lastPageSetNavigationButtonVisibility(Window window) {
        lastPageCachedOpenedState.setNavigationButtonVisibility(window);
    }

    public void twoOrMorePagesSetNavigationButtonVisibility(Window window) {
        twoOrMorePagesCachedState.setNavigationButtonVisibility(window);
    }
}
