package com.company.view.state;

import com.company.view.Window;

public class NoPageCachedState implements WebPageState {

    @Override
    public void setNavigationButtonVisibility(Window window) {
        window.disableButton(Window.FORWARD_BUTTON);
        window.disableButton(Window.BACK_BUTTON);
    }
}
