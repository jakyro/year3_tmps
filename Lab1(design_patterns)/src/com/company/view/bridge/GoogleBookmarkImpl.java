package com.company.view.bridge;

public class GoogleBookmarkImpl implements BookmarkImpl {
    @Override
    public String getUrl() {
        return "https://www.google.com";
    }

    @Override
    public String getName() {
        return "Google";
    }
}
