package com.company.view.bridge;

public class YoutubeBookmark extends Bookmark  {

    public YoutubeBookmark() {
        super(new YoutubeBookmarkImpl());
    }

    @Override
    public String getBookmarkName() {
        return bookmarkImpl.getName();
    }

    @Override
    public String getBookmarkUrl() {
        return bookmarkImpl.getUrl();
    }
}
