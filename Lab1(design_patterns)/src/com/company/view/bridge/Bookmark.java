package com.company.view.bridge;

public abstract class Bookmark {

    BookmarkImpl bookmarkImpl;

    Bookmark(BookmarkImpl bookmarkImpl) {
        this.bookmarkImpl = bookmarkImpl;
    }

    public abstract String getBookmarkName();

    public abstract String getBookmarkUrl();
}
