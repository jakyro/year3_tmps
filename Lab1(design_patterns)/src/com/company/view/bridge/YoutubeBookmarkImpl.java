package com.company.view.bridge;

public class YoutubeBookmarkImpl implements BookmarkImpl {
    @Override
    public String getUrl() {
        return "https://www.youtube.com";
    }

    @Override
    public String getName() {
        return "Youtube";
    }
}
