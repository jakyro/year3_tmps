package com.company.view.bridge;

public interface BookmarkImpl {
    public String getUrl();

    public String getName();
}
