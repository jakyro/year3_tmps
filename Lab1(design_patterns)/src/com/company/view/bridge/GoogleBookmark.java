package com.company.view.bridge;

public class GoogleBookmark extends Bookmark  {

    public GoogleBookmark() {
        super(new GoogleBookmarkImpl());
    }

    @Override
    public String getBookmarkName() {
        return bookmarkImpl.getName();
    }

    @Override
    public String getBookmarkUrl() {
        return bookmarkImpl.getUrl();
    }
}
