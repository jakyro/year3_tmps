package com.company.view.header;

import com.company.view.Window;

import javax.swing.*;

public class HeaderButtonFactory {
    static public JButton createButton(String type) {
        switch (type) {
            case Window.BACK_BUTTON:
                return new BackButton();
            case Window.FORWARD_BUTTON:
                return new ForwardButton();
            case Window.GO_BUTTON:
                return new GoButton();
            case Window.OK_BUTTON:
                return new OkButton();
        }
        throw new RuntimeException("No button found of type " + type);
    }
}
