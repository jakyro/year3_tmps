package com.company.view;

import javax.swing.*;

public class UrlFieldDecorator extends JTextField {
    private JTextField wrappee;
    UrlFieldDecorator(JTextField inputField) {
        this.wrappee = inputField;
    }

    @Override
    public String getText() {
        String text = wrappee.getText();
        if (hasProtocol(text))
            return text;
        return addProtocol(text);
    }

    private boolean hasProtocol(String text) {
        return text.startsWith("http://") || text.startsWith("https://");
    }

    private String addProtocol(String text) {
        return String.format("http://%s", text);
    }
}
