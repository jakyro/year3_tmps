package com.company.history;

public interface Storage
{
    void store(String url);
    void removeUrl(int index);
    String getUrl(int index);
    int numberOfUrls();
}
