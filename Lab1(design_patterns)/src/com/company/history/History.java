package com.company.history;

import com.company.history.builder.Director;
import com.company.history.builder.HistoryBuilder;

public class History {
    private static History history;

    private Storage storage;
    private int currentUrl;

    private History() {
        Director director = new Director();
        HistoryBuilder builder = new HistoryBuilder();
        storage = director.createStorage(builder);
        currentUrl = 0;
    }

    public static History getInstance() {
        if (history == null)
            history = new History();
        return history;
    }

    public String back() {
        String url = null;
        if (currentUrl > 1) {
            currentUrl--;
            url = storage.getUrl(currentUrl - 1);
        }
        return url;
    }

    public String forward() {
        String url = null;
        if (currentUrl != storage.numberOfUrls()) {
            currentUrl++;
            url = storage.getUrl(currentUrl - 1);
        }
        return url;
    }

    public void forward(String url) {
        currentUrl++;
        for (int i = currentUrl - 1; i < storage.numberOfUrls(); ) {
            storage.removeUrl(i);
        }
        storage.store(url);
    }

    public boolean isLast() {
        return currentUrl == storage.numberOfUrls();
    }

    public boolean isFirst() {
        return currentUrl <= 1 && storage.numberOfUrls() > 0;
    }

    public boolean isOnePageCached() {
        return currentUrl <= 1 && storage.numberOfUrls() == 1;
    }

    public boolean isNoPagesCached() {
        return currentUrl == 0 && storage.numberOfUrls() == 0;
    }
}
