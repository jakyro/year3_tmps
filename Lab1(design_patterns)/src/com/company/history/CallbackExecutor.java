package com.company.history;

public interface CallbackExecutor {

    void execute(byte[] bytes);
}
