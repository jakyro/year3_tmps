package com.company.history;

public class FileStorage extends Decorator
{
    public FileStorage(Storage storage)
    {
        super(storage);
    }

    public void store(String url)
    {
        super.store(url);
        System.out.println("FileStorage store");
    }

    @Override
    public void removeUrl(int index)
    {
        System.out.println("FileStorage remove");
        super.removeUrl(index);
    }

    @Override
    public String getUrl(int index)
    {
        System.out.println("FileStorage get");
        return super.getUrl(index);
    }

    @Override
    public int numberOfUrls()
    {
        System.out.println("FileStorage numberOfUrls");
        return super.numberOfUrls();
    }
}
