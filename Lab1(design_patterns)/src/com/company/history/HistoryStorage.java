package com.company.history;

import java.util.ArrayList;

public class HistoryStorage implements Storage
{
    private ArrayList<String> urls = new ArrayList<>();

    @Override
    public void store(String url) { urls.add(url); }

    public void removeUrl(int index) { urls.remove(index); }

    public String getUrl(int index) { return urls.get(index); }

    public int numberOfUrls() { return urls.size(); }
}
