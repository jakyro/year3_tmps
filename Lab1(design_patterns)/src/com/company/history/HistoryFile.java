package com.company.history;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.Channel;
import java.nio.channels.CompletionHandler;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class HistoryFile {
    private final Channel file;

    HistoryFile(Channel channel) {
        this.file = channel;
    }

    static HistoryFile async() {
        String filePath = "history.txt";
        Path file = Paths.get(filePath);
        AsynchronousFileChannel asyncFile = null;
        try {
            asyncFile = AsynchronousFileChannel.open(file,
                    StandardOpenOption.WRITE,
                    StandardOpenOption.READ,
                    StandardOpenOption.CREATE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new HistoryFile(asyncFile);
    }

    static HistoryFile sync() {
        String filePath = "history.txt";
        Path file = Paths.get(filePath);
        FileChannel syncFile = null;
        try {
            syncFile = FileChannel.open(file,
                    StandardOpenOption.WRITE,
                    StandardOpenOption.READ,
                    StandardOpenOption.CREATE);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new HistoryFile(syncFile);
    }

    void write(byte[] bytes, CallbackExecutor c) {
        if (file instanceof AsynchronousFileChannel) {
            ByteBuffer b = ByteBuffer.wrap(bytes);
            ((AsynchronousFileChannel) file).write(b, 0, b, completionHandler(c));
        } else if (file instanceof FileChannel) {
            try {
                ((FileChannel) file).write(ByteBuffer.wrap(bytes), 0);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    void read(CallbackExecutor c) {
        ByteBuffer b = ByteBuffer.allocate(2000);
        if (file instanceof AsynchronousFileChannel) {
            ((AsynchronousFileChannel) file).read(b, 0, b, completionHandler(c));
        } else if (file instanceof FileChannel) {
            try {
                ((FileChannel) file).read(b, 0);
                c.execute(b.array());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private CompletionHandler<Integer, ByteBuffer> completionHandler(CallbackExecutor c) {
        return new CompletionHandler<>() {
            @Override
            public void completed(Integer result, ByteBuffer attachment) {
                c.execute(attachment.array());
            }

            @Override
            public void failed(Throwable exc, ByteBuffer attachment) {

            }
        };
    }
}
