package com.company.history;

public class Decorator implements Storage {
    private Storage storage;

    public Decorator(Storage storage) {
        this.storage = storage;
    }

    public void store(String url) {
        storage.store(url);
    }

    public void removeUrl(int index) {
        storage.removeUrl(index);
    }

    public String getUrl(int index) {
        return storage.getUrl(index);
    }

    public int numberOfUrls() {
        return storage.numberOfUrls();
    }
}
