package com.company.history;

public class CacheStorage extends Decorator
{
    public CacheStorage(Storage storage)
    {
        super(storage);
    }

    public void store(String url)
    {
        super.store(url);
        System.out.println("CacheStorage store");
    }

    @Override
    public void removeUrl(int index)
    {
        System.out.println("CacheStorage index");
        super.removeUrl(index);
    }

    @Override
    public String getUrl(int index)
    {
        System.out.println("CacheStorage get");
        return super.getUrl(index);
    }

    @Override
    public int numberOfUrls()
    {
        System.out.println("CacheStorage numberOfUrls");
        return super.numberOfUrls();
    }
}
