package com.company.history.builder;

import com.company.history.Storage;

public class Director {
    public Storage createStorage(Builder builder){
        builder.addHistoryStorage();
        builder.addFileStorage();
        builder.addCachedStorage();
        return builder.build();
    }
}
