package com.company.history.builder;

import com.company.history.*;

public class HistoryBuilder implements Builder {
    public Storage storage;
    private Storage decoratorStorage = new Decorator(storage);

    public HistoryBuilder addCachedStorage(){
        decoratorStorage = new CacheStorage(decoratorStorage);
        return this;
    }

    public  HistoryBuilder addFileStorage(){
        decoratorStorage = new FileStorage(decoratorStorage);
        return this;
    }

    public HistoryBuilder addHistoryStorage(){
        decoratorStorage = new HistoryStorage();
        return this;
    }

    public Storage build(){
        Storage s = decoratorStorage;
        decoratorStorage = new Decorator(storage);
        return s;
    }

}

