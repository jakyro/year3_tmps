package com.company.history.builder;

import com.company.history.Storage;

public interface Builder {
    HistoryBuilder addCachedStorage();
    HistoryBuilder addFileStorage();
    HistoryBuilder addHistoryStorage();
    Storage build();
}