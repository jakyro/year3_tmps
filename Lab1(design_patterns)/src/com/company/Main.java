package com.company;

import com.company.history.History;
import com.company.view.ProxyWindow;
import com.company.view.Window;


public class Main {
    public static void main(String[] args) {
        new Main();
    }

    private Main() {
        ProxyWindow window = new ProxyWindow(new Window());
        window.updateState("");

        EventManager.subscribe(Window.BACK_BUTTON, data -> {
            String url = History.getInstance().back();
            window.updateState(url);
            window.loadUrl(url);
        });


        EventManager.subscribe(Window.GO_BUTTON, data -> {
            String url = data[0].toString();
            History.getInstance().forward(url);
            window.updateState(url);
            window.loadUrl(url);
        });


        EventManager.subscribe(Window.FORWARD_BUTTON, data -> {
            String url = History.getInstance().forward();
            window.updateState(url);
            window.loadUrl(url);
        });
    }
}